package com.hcl.Exception;

public class DataNotFoundException extends RuntimeException {

	public DataNotFoundException() {

		super("No data found");
	}

}
