package com.hcl.Exception;

public class CourseNotFoundException extends RuntimeException{
	public CourseNotFoundException (int id) {
		super(String.format("course with  Id %d not found ", id));
	} 
}
