package com.hcl.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.Exception.CourseNotFoundException;
import com.hcl.Exception.StudentNotFoundException;
import com.hcl.model.Course;
import com.hcl.model.Student;
import com.hcl.repositary.CourseRepositary;

@Service
public class CourseServiceImpl implements CourseService{

	@Autowired
	CourseRepositary courseRepositary;


	public void saveCourse(Course course) {

		courseRepositary.save(course);

	}

	public Course getCourseById(int id) {

		return courseRepositary.findById(id).orElseThrow(()-> new CourseNotFoundException(id));

	}

	public void deleteCourseById(int id) {

		courseRepositary.deleteById(id);

	}

	public List<Course> getAllCourses() {

		return courseRepositary.findAll();

	}

	@Override
	public void updateCourse(Course course) {
		courseRepositary.save(course);

	}
}
