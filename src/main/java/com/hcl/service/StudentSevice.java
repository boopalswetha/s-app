package com.hcl.service;

import java.util.List;

import com.hcl.model.Student;

public interface StudentSevice {

	void saveStudent(Student student);

	void updateStudent(Student student);

	List<Student> getAllStudents();

	Student getStudent(int id);

	void deleteStudent(int id);


}
