package com.hcl.service;

import java.util.List;

import com.hcl.model.Course;
import com.hcl.model.Student;

public interface CourseService {

	void saveCourse(Course course);

	void updateCourse(Course course);

	Course getCourseById(int id);

	List<Course> getAllCourses();

	void deleteCourseById(int id);
}
