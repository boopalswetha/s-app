package com.hcl.repositary;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.model.Course;
import com.hcl.model.Student;

@Repository
public interface CourseRepositary extends JpaRepository<Course, Integer>{

	

}
