package com.hcl.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Course;
import com.hcl.model.Student;
import com.hcl.service.CourseService;

@RestController
public class CourseController {
	@Autowired
	CourseService courseService;

	@PostMapping("/course")
	public ResponseEntity<String> saveCourse(@RequestBody Course course) {

		courseService.saveCourse(course);

		return new ResponseEntity<>("Course Created Sucessfully", HttpStatus.OK);

	}

	@PutMapping("/course")
	public ResponseEntity<String> UpdateCourse(@RequestBody Course course) {

		courseService.updateCourse(course);

		return new ResponseEntity<>("Course Updated Sucessfully", HttpStatus.OK);

	}

	@GetMapping("/course/{id}")
	public ResponseEntity<Course> getCourseById(@PathVariable int id) {

		Course course = courseService.getCourseById(id);

		return new ResponseEntity<>(course, HttpStatus.OK);

	}

	@GetMapping("/courses")
	public ResponseEntity<List<Course>> getAllCourese() {

		List<Course> courses = courseService.getAllCourses();

		return new ResponseEntity<>(courses, HttpStatus.OK);

	}

	@DeleteMapping("/course/{id}")
	public ResponseEntity<String> deleteCourseById(@PathVariable int id) {

		courseService.deleteCourseById(id);

		return new ResponseEntity<>("Course deleted Sucessfully", HttpStatus.OK);

	}
}
