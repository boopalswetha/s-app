package com.hcl.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.model.Student;
import com.hcl.service.StudentSevice;

@RestController
public class StudentController {

	@Autowired
	private StudentSevice studentService;

	@PostMapping("/student")
	public ResponseEntity<String> saveStudent(@RequestBody Student student) {

		studentService.saveStudent(student);
		return new ResponseEntity<>("Student Created Sucessfully", HttpStatus.OK);

	}

	@PutMapping("/student")
	public ResponseEntity<String> UpdateStudent(@RequestBody Student student) {

		studentService.updateStudent(student);
		return new ResponseEntity<>("student Updated Sucessfully", HttpStatus.OK);
	}

	@GetMapping("/student/{id}")
	public ResponseEntity<Student> getStudent(@PathVariable int id) {

		Student student = studentService.getStudent(id);
		return new ResponseEntity<>(student, HttpStatus.OK);
	}

	@GetMapping("/students")
	public ResponseEntity<List<Student>> getAllStudents() {

		List<Student> students = studentService.getAllStudents();
		return new ResponseEntity<>(students, HttpStatus.OK);
	}

	@DeleteMapping("/student/{id}")
	public ResponseEntity<String> deleteStudent(@PathVariable int id) {

		studentService.deleteStudent(id);

		return new ResponseEntity<>("Student deleted Sucessfully", HttpStatus.OK);

	}

}
