package com.hcl.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.aspectj.weaver.NewConstructorTypeMunger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.OngoingStubbing;

import com.hcl.model.Course;

import com.hcl.model.Student;

import com.hcl.repositary.StudentRepositary;



@RunWith(MockitoJUnitRunner.Silent.class)
public class StudentServiceTest {
	@InjectMocks
	StudentServiceImpl studentServiceImpl;
	
	@Mock
	StudentRepositary studentRepositary;
	
	
	

	
	@Test
	public void testReadById() {
		Student student=new Student();
		student.setStudentId(123);
		student.setStudentName("saikrishna");
		Mockito.when(studentRepositary.findById(student.getStudentId())).thenReturn(Optional.of(student));

		Student p1 = studentServiceImpl.getStudent(student.getStudentId());
		Assert.assertNotNull(p1);

	}
	
	
	@Test
	public void testGetAll() {
		List<Student> students= new ArrayList<Student>();
		Student student=new Student();

		students.add(student);
		student.setStudentId(123);
		student.setStudentName("saikrishna");
		Mockito.when(studentRepositary.findAll()).thenReturn(students);

		List<Student> p1 = studentServiceImpl.getAllStudents();
		Assert.assertNotNull(p1);

	}
	
	@Test
	public void testReadByIdpositive() {
		Student student=new Student();

		student.setStudentId(123);
		student.setStudentName("saikrishna");
		Mockito.when(studentRepositary.findById(student.getStudentId())).thenReturn(Optional.of(student));

		Student p1 = studentServiceImpl.getStudent(student.getStudentId());
		Assert.assertEquals(p1, student);;

	}
	
	
	@Test
	public void testGetAllpositive() {
		List<Student> students= new ArrayList<Student>();
		Student	student=new Student();

		students.add(student);
		student.setStudentId(123);
		student.setStudentName("saikrishna");
		Mockito.when(studentRepositary.findAll()).thenReturn(students);

		List<Student> p1 = studentServiceImpl.getAllStudents();
		Assert.assertEquals(p1, students);

	}
	
	
}
